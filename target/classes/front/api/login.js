function loginApi(data) {
    return $axios({
      'url': '/user/login',
      'method': 'post',
      data
    })
  }

function loginoutApi() {
  return $axios({
    'url': '/user/loginout',
    'method': 'post',
  })
}

function sendMsgApi(data) {
    return $axios({
        'url':'/user/sendMsg',
        'method':'post',
        data
    })
}

//获取登录用户的基本信息
function LoginDataApi(data){
    return $axios({
        'url': '/user/loginData',
        'method': 'get',
        data
    })
}

  