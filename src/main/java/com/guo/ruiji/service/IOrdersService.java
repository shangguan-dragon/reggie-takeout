package com.guo.ruiji.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.guo.ruiji.dto.OrdersDto;
import com.guo.ruiji.entity.Orders;
import com.baomidou.mybatisplus.extension.service.IService;

import java.time.LocalDateTime;

/**
 * <p>
 * 订单表 服务类
 * </p>
 *
 * @author guo
 * @since 2022-04-18
 */
public interface IOrdersService extends IService<Orders> {

    /**
     * 用户下单方法
     * @param orders
     */
    void submit(Orders orders);

    /**
     * 用户查看自己的订单
     * @param page
     * @param pageSize
     * @return
     */
    Page<Orders> userPage(int page, int pageSize);

    /**
     * 管理员查看订单
     * @param page
     * @param pageSize
     * @param number
     * @param beginTime
     * @param endTime
     * @return
     */
    Page<OrdersDto> empPage(int page, int pageSize,String number,
                   LocalDateTime beginTime, LocalDateTime endTime);

    /**
     * 再来一单
     * @param order
     */
    void again(Orders order);
}
