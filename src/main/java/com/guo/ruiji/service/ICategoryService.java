package com.guo.ruiji.service;

import com.guo.ruiji.entity.Category;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 菜品及套餐分类 服务类
 * </p>
 *
 * @author guo
 * @since 2022-04-18
 */
public interface ICategoryService extends IService<Category> {

    /**
     * 自定义删除分类方法，只有无关联才能删除
     * @param id
     */
    void remove(Long id);
}
