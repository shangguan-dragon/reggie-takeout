package com.guo.ruiji.service;

import com.guo.ruiji.entity.DishFlavor;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 菜品口味关系表 服务类
 * </p>
 *
 * @author guo
 * @since 2022-04-18
 */
public interface IDishFlavorService extends IService<DishFlavor> {

}
