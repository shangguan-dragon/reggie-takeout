package com.guo.ruiji.service;

import com.guo.ruiji.dto.SetmealDto;
import com.guo.ruiji.entity.Setmeal;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 套餐 服务类
 * </p>
 *
 * @author guo
 * @since 2022-04-18
 */
public interface ISetmealService extends IService<Setmeal> {

    //新增套餐，并且添加菜品集合两张表操作
    void saveWithDish(SetmealDto setmealDto);

    //根据id查询套餐以及菜品信息
    SetmealDto getByIdWithDish(Long id);

    //更新套餐信息和对应菜品信息
    void updateWithDish(SetmealDto setmealDto);
}
