package com.guo.ruiji.service;

import com.guo.ruiji.dto.DishDto;
import com.guo.ruiji.entity.Dish;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 菜品管理 服务类
 * </p>
 *
 * @author guo
 * @since 2022-04-18
 */
public interface IDishService extends IService<Dish> {

    //新增菜品，同时增加口味信息
    void saveWithFlavor(DishDto dishDto);

    //查两张表，含口味信息
    DishDto getByIdWithFlavor(Long id);

    //更新菜品信息和对应口味信息
    void updateWithFlavor(DishDto dishDto);
}
