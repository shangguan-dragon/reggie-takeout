package com.guo.ruiji.service;

import com.guo.ruiji.entity.SetmealDish;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 套餐菜品关系 服务类
 * </p>
 *
 * @author guo
 * @since 2022-04-18
 */
public interface ISetmealDishService extends IService<SetmealDish> {

}
