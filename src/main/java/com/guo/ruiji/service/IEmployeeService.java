package com.guo.ruiji.service;

import com.guo.ruiji.entity.Employee;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 员工信息 服务类
 * </p>
 *
 * @author guo
 * @since 2022-04-18
 */
public interface IEmployeeService extends IService<Employee> {

}
