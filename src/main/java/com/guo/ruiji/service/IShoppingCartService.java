package com.guo.ruiji.service;

import com.guo.ruiji.entity.ShoppingCart;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 购物车 服务类
 * </p>
 *
 * @author guo
 * @since 2022-04-18
 */
public interface IShoppingCartService extends IService<ShoppingCart> {
    ShoppingCart viewShopCart(ShoppingCart shoppingCart);
}
