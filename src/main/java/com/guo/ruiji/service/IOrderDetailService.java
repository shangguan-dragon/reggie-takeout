package com.guo.ruiji.service;

import com.guo.ruiji.entity.OrderDetail;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 订单明细表 服务类
 * </p>
 *
 * @author guo
 * @since 2022-04-18
 */
public interface IOrderDetailService extends IService<OrderDetail> {

}
