package com.guo.ruiji.service;

import com.guo.ruiji.entity.AddressBook;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 地址管理 服务类
 * </p>
 *
 * @author guo
 * @since 2022-04-18
 */
public interface IAddressBookService extends IService<AddressBook> {

}
