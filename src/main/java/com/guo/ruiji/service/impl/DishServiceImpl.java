package com.guo.ruiji.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.guo.ruiji.dto.DishDto;
import com.guo.ruiji.entity.Dish;
import com.guo.ruiji.entity.DishFlavor;
import com.guo.ruiji.mapper.DishMapper;
import com.guo.ruiji.service.IDishFlavorService;
import com.guo.ruiji.service.IDishService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 菜品管理 服务实现类
 * </p>
 *
 * @author guo
 * @since 2022-04-18
 */
@Service
public class DishServiceImpl extends ServiceImpl<DishMapper, Dish> implements IDishService {

    @Autowired
    private IDishFlavorService dishFlavorService;
    /**
     * 新增菜品 同时保存对应口味数据
     * @param dishDto
     */
    @Transactional
    @Override
    public void saveWithFlavor(DishDto dishDto) {
        //保存菜品的基本信息到dish表中
        this.save(dishDto);
        saveFlavor(dishDto);
    }

    /**
     * 查询两张数据表
     * @param id
     * @return
     */
    @Override
    public DishDto getByIdWithFlavor(Long id) {
        //查询菜品基本信息，从dish表上查
        Dish dish = this.getById(id);
        //查询当前菜品对应的口味信息，从dish——flavor表上查
        LambdaQueryWrapper<DishFlavor> dishFlavorWrapper = new LambdaQueryWrapper<>();
        dishFlavorWrapper.eq(DishFlavor::getDishId,dish.getId());
        List<DishFlavor> flavors = dishFlavorService.list(dishFlavorWrapper);
        //开始拷贝
        DishDto dishDto = new DishDto();
        BeanUtil.copyProperties(dish,dishDto);
        dishDto.setFlavors(flavors);
        return dishDto;
    }

    /**
     * 更新菜品和口味信息
     * @param dishDto
     */
    @Transactional
    @Override
    public void updateWithFlavor(DishDto dishDto) {
        //更新菜品基本信息
        this.updateById(dishDto);
        //更新口味信息，先删除dishId的口味
        LambdaQueryWrapper<DishFlavor> dishDtoWrapper = new LambdaQueryWrapper<>();
        dishDtoWrapper.eq(DishFlavor::getDishId,dishDto.getId());
        dishFlavorService.remove(dishDtoWrapper);
        //再添加对应dishId的口味
        saveFlavor(dishDto);
    }

    /**
     * 批量保存菜品口味信息通用方法
     * @param dishDto
     */
    private void saveFlavor(DishDto dishDto){
        Long dishId = dishDto.getId();//菜品id
        //菜品口味
        List<DishFlavor> flavors = dishDto.getFlavors();
        flavors=flavors.stream().map(item->{
            item.setDishId(dishId);
            return item;
        }).collect(Collectors.toList());
        //批量保存菜品口味到dish_flavor表中
        dishFlavorService.saveBatch(flavors);
    }
}
