package com.guo.ruiji.service.impl;

import com.guo.ruiji.entity.Employee;
import com.guo.ruiji.mapper.EmployeeMapper;
import com.guo.ruiji.service.IEmployeeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 员工信息 服务实现类
 * </p>
 *
 * @author guo
 * @since 2022-04-18
 */
@Service
public class EmployeeServiceImpl extends ServiceImpl<EmployeeMapper, Employee> implements IEmployeeService {

}
