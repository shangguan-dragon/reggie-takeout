package com.guo.ruiji.service.impl;

import com.guo.ruiji.entity.DishFlavor;
import com.guo.ruiji.mapper.DishFlavorMapper;
import com.guo.ruiji.service.IDishFlavorService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 菜品口味关系表 服务实现类
 * </p>
 *
 * @author guo
 * @since 2022-04-18
 */
@Service
public class DishFlavorServiceImpl extends ServiceImpl<DishFlavorMapper, DishFlavor> implements IDishFlavorService {

}
