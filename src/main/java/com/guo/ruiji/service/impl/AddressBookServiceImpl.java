package com.guo.ruiji.service.impl;

import com.guo.ruiji.entity.AddressBook;
import com.guo.ruiji.mapper.AddressBookMapper;
import com.guo.ruiji.service.IAddressBookService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 地址管理 服务实现类
 * </p>
 *
 * @author guo
 * @since 2022-04-18
 */
@Service
public class AddressBookServiceImpl extends ServiceImpl<AddressBookMapper, AddressBook> implements IAddressBookService {

}
