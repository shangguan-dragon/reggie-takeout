package com.guo.ruiji.service.impl;

import com.guo.ruiji.entity.OrderDetail;
import com.guo.ruiji.mapper.OrderDetailMapper;
import com.guo.ruiji.service.IOrderDetailService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 订单明细表 服务实现类
 * </p>
 *
 * @author guo
 * @since 2022-04-18
 */
@Service
public class OrderDetailServiceImpl extends ServiceImpl<OrderDetailMapper, OrderDetail> implements IOrderDetailService {

}
