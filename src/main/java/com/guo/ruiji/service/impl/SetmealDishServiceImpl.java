package com.guo.ruiji.service.impl;

import com.guo.ruiji.entity.SetmealDish;
import com.guo.ruiji.mapper.SetmealDishMapper;
import com.guo.ruiji.service.ISetmealDishService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 套餐菜品关系 服务实现类
 * </p>
 *
 * @author guo
 * @since 2022-04-18
 */
@Service
public class SetmealDishServiceImpl extends ServiceImpl<SetmealDishMapper, SetmealDish> implements ISetmealDishService {

}
