package com.guo.ruiji.service.impl;

import com.guo.ruiji.entity.User;
import com.guo.ruiji.mapper.UserMapper;
import com.guo.ruiji.service.IUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户信息 服务实现类
 * </p>
 *
 * @author guo
 * @since 2022-04-18
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {

}
