package com.guo.ruiji.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.guo.ruiji.common.BaseContext;
import com.guo.ruiji.common.CustomException;
import com.guo.ruiji.dto.OrdersDto;
import com.guo.ruiji.entity.*;
import com.guo.ruiji.mapper.OrdersMapper;
import com.guo.ruiji.service.*;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

/**
 * <p>
 * 订单表 服务实现类
 * </p>
 *
 * @author guo
 * @since 2022-04-18
 */
@Service
public class OrdersServiceImpl extends ServiceImpl<OrdersMapper, Orders> implements IOrdersService {

    @Autowired
    private IShoppingCartService shoppingCartService;

    @Autowired
    private IUserService userService;

    @Autowired
    private IAddressBookService addressBookService;

    @Autowired
    private IOrderDetailService orderDetailService;

    /**
     * 用户下单
     *
     * @param orders
     */
    @Transactional
    @Override
    public void submit(Orders orders) {


        //获得当前用户id
        Long userId = BaseContext.getCurrentId();
        //查询当前购物用户的购物车数据
        LambdaQueryWrapper<ShoppingCart> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(ShoppingCart::getUserId, userId);
        List<ShoppingCart> shoppingCartList = shoppingCartService.list(queryWrapper);
        //判断购物车
        if (StrUtil.isBlankIfStr(shoppingCartList)) {
            throw new CustomException("购物车为空，不能下单");
        }

        //要先设置订单id
        long orderId = IdUtil.getSnowflake(1, 20).nextId();
        //设置初值=0，累加
        AtomicInteger amount = new AtomicInteger(0);

        //遍历购物车数据
        List<OrderDetail> orderDetais = shoppingCartList.stream().map(item -> {
            OrderDetail orderDetail = new OrderDetail();
            orderDetail.setOrderId(orderId);
            //只要属性名称一致(number,name)保持一致就能copy，和对象的类型无关
            BeanUtil.copyProperties(item, orderDetail);
            //订单总金额 += 单价*数量
            amount.addAndGet(item.getAmount().multiply(new BigDecimal(item.getNumber())).intValue());
            return orderDetail;
        }).collect(Collectors.toList());

        //查询用户数据
        User user = userService.getById(userId);
        //查询地址数据
        Long addressBookId = orders.getAddressBookId();//查到地址的id
        AddressBook addressBook = addressBookService.getById(addressBookId);
        if (StrUtil.isBlankIfStr(addressBook)) {
            throw new CustomException("地址信息有误，不能下单");
        }

        //向订单表插入数据   一条数据

        orders.setNumber(Convert.toStr(orderId));
        orders.setId(orderId);
        orders.setOrderTime(LocalDateTime.now());
        orders.setCheckoutTime(LocalDateTime.now());
        orders.setStatus(2);//2待派送
        orders.setAmount(new BigDecimal(amount.get()));//总金额
        orders.setUserId(userId);
        orders.setUserName(user.getName());
        orders.setConsignee(addressBook.getConsignee());
        orders.setPhone(addressBook.getPhone());
        orders.setAddress((addressBook.getProvinceName() == null ? "" : addressBook.getProvinceName())
                + (addressBook.getCityName() == null ? "" : addressBook.getCityName())
                + (addressBook.getDistrictName() == null ? "" : addressBook.getDistrictName())
                + (addressBook.getDetail() == null ? "" : addressBook.getDetail()));
        this.save(orders); //insert orders

        //向订单明细表插入多条数据   多条数据
        orderDetailService.saveBatch(orderDetais);

        //清空购物车
        shoppingCartService.remove(queryWrapper);
    }

    /**
     * 用户查看自己的订单
     *
     * @param page
     * @param pageSize
     * @return
     */
    @Override
    public Page<Orders> userPage(int page, int pageSize) {
        Page<Orders> pageInfo = new Page<>(page, pageSize);
        //构建查询条件
        LambdaQueryWrapper<Orders> queryWrapper = new LambdaQueryWrapper<>();
        //用户id不为空，则查新指定用户的id
        queryWrapper.eq(Orders::getUserId, BaseContext.getCurrentId());

        //查询当前登录用户订单列表
        this.page(pageInfo, queryWrapper);

        List<Orders> list = pageInfo.getRecords().stream().map(item -> {
            //查询当前订单的详情： 菜品/套餐
            List<OrderDetail> details = orderDetailService.list(Wrappers.
                    <OrderDetail>lambdaQuery().eq(OrderDetail::getOrderId, item.getId()));
            item.setOrderDetails(details);

            return item;
        }).collect(Collectors.toList());
        pageInfo.setRecords(list);
        return pageInfo;
    }

    /**
     * 管理员查询订单
     *
     * @param page
     * @param pageSize
     * @param number
     * @param beginTime
     * @param endTime
     * @return
     */
    @Override
    public Page<OrdersDto> empPage(int page, int pageSize, String number, LocalDateTime beginTime, LocalDateTime endTime) {
        LocalDateTime localDateTimeBegin = null;
        LocalDateTime localDateTimeEnd = null;
        // 对其时间参数进行处理
        if (beginTime != null && endTime != null) {
            // beginTime处理
            Instant instant = beginTime.toInstant(ZoneOffset.UTC);
            ZoneId zoneId = ZoneId.systemDefault();
            localDateTimeBegin = instant.atZone(zoneId).toLocalDateTime();
            //formatBeginTime = localDateTime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));

            // endTime 进行处理
            Instant instant1 = endTime.toInstant(ZoneOffset.UTC);
            ZoneId zoneId1 = ZoneId.systemDefault();
            localDateTimeEnd = instant1.atZone(zoneId1).toLocalDateTime();
            //formatEndTime = localDateTime1.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        }

        Page<Orders> pageInfo = new Page<>(page, pageSize);
        Page<OrdersDto> pageDto = new Page<>();

        QueryWrapper<Orders> wrapper = new QueryWrapper<>();
        if (!StrUtil.isBlankIfStr(number)) {
            wrapper.eq("number", number);
        }
        if (!StrUtil.isBlankIfStr(localDateTimeBegin)) {
            wrapper.ge("order_time", localDateTimeBegin);
        }
        if (!StrUtil.isBlankIfStr(localDateTimeEnd)) {
            wrapper.le("order_time", localDateTimeEnd);
        }
        wrapper.orderByDesc("order_time");
        this.page(pageInfo, wrapper);
        // 将其除了records中的内存复制到pageDto中
        BeanUtil.copyProperties(pageInfo, pageDto, "records");

        List<Orders> records = pageInfo.getRecords();

        List<OrdersDto> collect = records.stream().map((order) -> {
            OrdersDto ordersDto = new OrdersDto();

            BeanUtil.copyProperties(order, ordersDto);
            // 根据订单id查询订单详细信息

            QueryWrapper<OrderDetail> wrapperDetail = new QueryWrapper<>();
            wrapperDetail.eq("order_id", order.getId());

            List<OrderDetail> orderDetails = orderDetailService.list(wrapperDetail);
            ordersDto.setOrderDetails(orderDetails);

            // 根据userId 查询用户姓名
            Long userId = order.getUserId();
            User user = userService.getById(userId);
            ordersDto.setUserName(user.getName());
            ordersDto.setPhone(user.getPhone());

            // 获取地址信息
            Long addressBookId = order.getAddressBookId();
            AddressBook addressBook = addressBookService.getById(addressBookId);
            ordersDto.setAddress(addressBook.getDetail());
            ordersDto.setConsignee(addressBook.getConsignee());

            return ordersDto;
        }).collect(Collectors.toList());

        pageDto.setRecords(collect);

        return pageDto;
    }

    /**
     * 再来一单
     * @param order
     */
    @Override
    public void again(Orders order) {
        //获取订单里的订单号
        Orders orders = this.getById(order.getId());
        String number = orders.getNumber();

        //根据条件查询
        LambdaQueryWrapper<OrderDetail> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(OrderDetail::getOrderId, number);
        List<OrderDetail> orderDetails = orderDetailService.list(wrapper);

        //根据查到的数据再次添加到购物车里
        List<ShoppingCart> shoppingCarts = orderDetails.stream().map((item) -> {
            ShoppingCart shoppingCart = new ShoppingCart();
            shoppingCart.setName(item.getName());
            shoppingCart.setImage(item.getImage());
            shoppingCart.setUserId(BaseContext.getCurrentId());
            shoppingCart.setDishId(item.getDishId());
            shoppingCart.setSetmealId(item.getSetmealId());
            shoppingCart.setDishFlavor(item.getDishFlavor());
            shoppingCart.setNumber(item.getNumber());
            shoppingCart.setAmount(item.getAmount());
            return shoppingCart;
        }).collect(Collectors.toList());

        shoppingCartService.saveBatch(shoppingCarts);
    }
}
