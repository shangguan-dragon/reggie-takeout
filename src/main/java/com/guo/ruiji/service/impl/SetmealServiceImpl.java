package com.guo.ruiji.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.guo.ruiji.dto.DishDto;
import com.guo.ruiji.dto.SetmealDto;
import com.guo.ruiji.entity.Dish;
import com.guo.ruiji.entity.DishFlavor;
import com.guo.ruiji.entity.Setmeal;
import com.guo.ruiji.entity.SetmealDish;
import com.guo.ruiji.mapper.SetmealMapper;
import com.guo.ruiji.service.ISetmealDishService;
import com.guo.ruiji.service.ISetmealService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 套餐 服务实现类
 * </p>
 *
 * @author guo
 * @since 2022-04-18
 */
@Service
public class SetmealServiceImpl extends ServiceImpl<SetmealMapper, Setmeal> implements ISetmealService {

    @Autowired
    private ISetmealDishService setmealDishService;

    /**
     * 新增套餐，并且添加菜品集合 两张表操作
     *
     * @param setmealDto
     */
    @Transactional
    @Override
    public void saveWithDish(SetmealDto setmealDto) {
        //保存菜品的基本信息到dish表中
        this.save(setmealDto);
        saveDishs(setmealDto);
    }

    /**
     * 根据id查询套餐以及菜品信息
     * @param id
     * @return
     */
    @Override
    public SetmealDto getByIdWithDish(Long id) {
        //查询套餐基本信息，从setmeal表上查
        Setmeal setmeal = this.getById(id);
        //查询当前套餐对应的菜品信息，从setmeal_dish表上查
        LambdaQueryWrapper<SetmealDish> setmealDishQueryWrapper = new LambdaQueryWrapper<>();
        setmealDishQueryWrapper.eq(SetmealDish::getSetmealId,setmeal.getId());
        List<SetmealDish> setmealDishes = setmealDishService.list(setmealDishQueryWrapper);
        //开始拷贝
        SetmealDto setmealDto = new SetmealDto();
        BeanUtil.copyProperties(setmeal,setmealDto);
        setmealDto.setSetmealDishes(setmealDishes);
        return setmealDto;
    }

    /**
     * 更新套餐信息和对应菜品信息
     * @param setmealDto
     */
    @Transactional
    @Override
    public void updateWithDish(SetmealDto setmealDto) {
        //更新套餐基本信息
        this.updateById(setmealDto);
        //更新菜品信息，先删除setmealId的口味
        LambdaQueryWrapper<SetmealDish> setmealDishWrapper = new LambdaQueryWrapper<>();
        setmealDishWrapper.eq(SetmealDish::getSetmealId,setmealDto.getId());
        setmealDishService.remove(setmealDishWrapper);
        //再添加对应setmealId的口味
        saveDishs(setmealDto);
    }

    /**
     * 批量保存套餐菜品信息通用方法
     * @param setmealDto
     */
    private void saveDishs(SetmealDto setmealDto) {
        Long setmealId = setmealDto.getId();//套餐id
        //菜品类型
        List<SetmealDish> setmealDishes = setmealDto.getSetmealDishes();
        setmealDishes = setmealDishes.stream().map(item -> {
            item.setSetmealId(setmealId);
            return item;
        }).collect(Collectors.toList());
        //批量保存菜品到setmeal_dish表中
        setmealDishService.saveBatch(setmealDishes);
    }
}
