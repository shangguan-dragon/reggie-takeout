package com.guo.ruiji.service;

import com.guo.ruiji.entity.User;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户信息 服务类
 * </p>
 *
 * @author guo
 * @since 2022-04-18
 */
public interface IUserService extends IService<User> {

}
