package com.guo.ruiji.controller;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.guo.ruiji.common.BaseContext;
import com.guo.ruiji.common.R;
import com.guo.ruiji.entity.ShoppingCart;
import com.guo.ruiji.service.IShoppingCartService;
import com.guo.ruiji.service.impl.ShoppingCartServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 购物车 前端控制器
 * </p>
 *
 * @author guo
 * @since 2022-04-18
 */
@RestController
@RequestMapping("/shoppingCart")
public class ShoppingCartController {

    @Autowired
    private IShoppingCartService shoppingCartService;

    /**
     * 添加购物车
     *
     * @param shoppingCart
     * @return
     */
    @PostMapping("/add")
    public R<ShoppingCart> add(@RequestBody ShoppingCart shoppingCart) {
        ShoppingCart cartServiceOne = shoppingCartService.viewShopCart(shoppingCart);
        if (cartServiceOne != null) {
            //如果已经存在 就在原来的基础上+1  update xxx set number = number + 1
            Integer number = cartServiceOne.getNumber();
            cartServiceOne.setNumber(number + 1);
            shoppingCartService.updateById(cartServiceOne);
        } else {
            //如果不存在 则添加到购物车  数量默认为1  insert
            shoppingCart.setNumber(1);
            shoppingCartService.save(shoppingCart);
            cartServiceOne = shoppingCart;
        }
        return R.success(cartServiceOne);
    }

    /**
     * 查看购物车
     *
     * @return
     */
    @GetMapping("/list")
    public R<List<ShoppingCart>> list() {
        LambdaQueryWrapper<ShoppingCart> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(ShoppingCart::getUserId, BaseContext.getCurrentId());
        queryWrapper.orderByDesc(ShoppingCart::getCreateTime);
        List<ShoppingCart> list = shoppingCartService.list(queryWrapper);
        return R.success(list);
    }

    @PostMapping("/sub")
    public R<ShoppingCart> sub(@RequestBody ShoppingCart shoppingCart) {
        //两种情况    1.操作的是菜品    {dishId: "1397860792492666881",setmealId: null}
        //          2.操作的是套餐    {dishId: null, setmealId: "1512985519641460737"}

        //获取用户id  看当前操作的购物车是哪个用户的
        ShoppingCart cartServiceOne = shoppingCartService.viewShopCart(shoppingCart);

        if (cartServiceOne != null) {
            Integer number = cartServiceOne.getNumber();
            //数量大于1 则 -1
            if (number > 1) {
                cartServiceOne.setNumber(number - 1);
                shoppingCartService.updateById(cartServiceOne);
            }
            //数量为1 则直接删除该项
            if (number == 1) {
                shoppingCartService.removeById(cartServiceOne);
                //给前端用，让页面不再显示数量
                cartServiceOne.setNumber(0);
            }
        } else {
            R.error("购物车没有该项");
        }

        return R.success(cartServiceOne);
    }

    /**
     * 清空购物车
     * @return
     */
    @DeleteMapping("/clean")
    public R<String> clean(){
        //delete from shopping_cart where user_id = ?
        LambdaQueryWrapper<ShoppingCart> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(ShoppingCart::getUserId,BaseContext.getCurrentId());

        shoppingCartService.remove(queryWrapper);

        return R.success("清空购物车成功");
    }

}
