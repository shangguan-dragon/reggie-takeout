package com.guo.ruiji.controller;


import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.guo.ruiji.common.CustomException;
import com.guo.ruiji.common.R;
import com.guo.ruiji.dto.DishDto;
import com.guo.ruiji.entity.Category;
import com.guo.ruiji.entity.Dish;
import com.guo.ruiji.entity.DishFlavor;
import com.guo.ruiji.service.ICategoryService;
import com.guo.ruiji.service.IDishFlavorService;
import com.guo.ruiji.service.IDishService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 菜品管理 前端控制器
 * </p>
 *
 * @author guo
 * @since 2022-04-18
 */
@Slf4j
@RestController
@RequestMapping("/dish")
public class DishController {

    @Autowired
    private IDishService dishService;

    @Autowired
    private ICategoryService categoryService;

    @Autowired
    private IDishFlavorService dishFlavorService;

    /**
     * 菜品信息分页查询
     *
     * @param page     当前查询页码
     * @param pageSize 每页展示记录数
     * @param name     菜名名称 - 可选参数
     * @return
     */
    @GetMapping("/page")
    public R<Page> page(int page, int pageSize, String name) {
        log.info("page = {},pageSize = {},name = {}", page, pageSize, name);
        //构造分页构造器
        Page<Dish> pageInfo = new Page<>(page, pageSize);
        Page<DishDto> dishDtoPage = new Page<>();

        //构造条件构造器
        LambdaQueryWrapper<Dish> queryWrapper = new LambdaQueryWrapper<>();
        //添加过滤条件
        queryWrapper.like(StringUtils.isNotBlank(name), Dish::getName, name);
        //添加排序条件
        queryWrapper.orderByDesc(Dish::getUpdateTime);
        //只查isDelete=0的数据
        queryWrapper.eq(Dish::getIsDeleted, 0);

        //执行查询
        dishService.page(pageInfo, queryWrapper);
        //对象拷贝,分页数据
        BeanUtil.copyProperties(pageInfo, dishDtoPage, "records");
        List<Dish> records = pageInfo.getRecords();
        //两个数据的集合
        List<DishDto> list = records.stream().map(item -> {
            DishDto dishDto = new DishDto();//此时其他属性除categoryName为null，需拷贝
            BeanUtil.copyProperties(item, dishDto);
            Long categoryId = item.getCategoryId();//拿到分类id
            Category category = categoryService.getById(categoryId);
            String categoryName = category.getName();
            dishDto.setCategoryName(categoryName);
            return dishDto;
        }).collect(Collectors.toList());
        dishDtoPage.setRecords(list);
        return R.success(dishDtoPage);
    }

    /**
     * 新增菜品
     *
     * @param dishDto
     * @return
     */
    @PostMapping
    public R<String> save(@RequestBody DishDto dishDto) {
        log.info(dishDto.toString());
        dishService.saveWithFlavor(dishDto);
        return R.success("新增菜品成功");
    }

    /**
     * 根据id查菜品信息
     *
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    public R<DishDto> getByIdWithFlavor(@PathVariable Long id) {
        DishDto dishDto = dishService.getByIdWithFlavor(id);
        if (dishDto != null) {
            return R.success(dishDto);
        }
        return R.error("未查找到菜品信息");
    }

    /**
     * 修改菜品和口味信息
     *
     * @param dishDto
     * @return
     */
    @PutMapping
    public R<String> update(@RequestBody DishDto dishDto) {
        dishService.updateWithFlavor(dishDto);
        return R.success("修改菜品信息成功");
    }

    /**
     * （可批量）1启售  0停售
     *
     * @param flag
     * @return
     */
    @PostMapping("/status/{flag}")
    public R<String> status(@PathVariable Integer flag, String[] ids) {
        if (ids == null || ids.length == 0) {
            throw new CustomException("参数有误");
        }
        LambdaUpdateWrapper<Dish> updateWrapper = new LambdaUpdateWrapper<>();
        // set()为sql中的set,in()为sql中的in
        updateWrapper.set(Dish::getStatus, flag).in(Dish::getId, ids);
        dishService.update(updateWrapper);
        return R.success(flag == 0 ? "已停售" : "已启售");
    }

    /**
     * 批量删除菜品，删除单个菜品
     *
     * @param
     * @return
     */
    @DeleteMapping
    public R<String> delete(String[] ids) {
        if (ids == null || ids.length == 0) {
            throw new CustomException("参数有误");
        }
        List<Dish> dishes = dishService.listByIds(Arrays.asList(ids));
        for (Dish dish : dishes) {
            if (dish.getStatus() != 0) {
                throw new CustomException(dish.getName() + "为启售状态，不能删除");
            }
        }
        LambdaUpdateWrapper<Dish> updateWrapper = new LambdaUpdateWrapper<>();
        // set()为sql中的set,in()为sql中的in
        updateWrapper.set(Dish::getIsDeleted, 1).in(Dish::getId, ids);
        dishService.update(updateWrapper);
        return R.success("删除成功");
    }

    /**
     * 根据条件查询菜品信息
     *
     * @param dish
     * @return
     */
    /*@GetMapping("/list")
    public R<List<Dish>>list(Dish dish){
        LambdaQueryWrapper<Dish> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(!StrUtil.isBlankIfStr(dish.getCategoryId()),Dish::getCategoryId,dish.getCategoryId());
        //添加排序条件
        queryWrapper.orderByAsc(Dish::getSort).orderByDesc(Dish::getUpdateTime);
        List<Dish> list = dishService.list(queryWrapper);
        return R.success(list);
    }*/
    //改在list根据条件查询菜品信息
    @GetMapping("/list")
    public R<List<DishDto>> list(Dish dish) {
        LambdaQueryWrapper<Dish> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(dish.getCategoryId()!=null, Dish::getCategoryId, dish.getCategoryId());
        queryWrapper.eq(Dish::getIsDeleted,0);
        //添加排序条件
        queryWrapper.orderByAsc(Dish::getSort).orderByDesc(Dish::getUpdateTime);
        List<Dish> list = dishService.list(queryWrapper);
        //查根据菜品对应的口味信息  两个数据的集合
        List<DishDto> dishDtoList = list.stream().map(item -> {
            DishDto dishDto = new DishDto();//此时其他属性除categoryName为null，需拷贝
            BeanUtil.copyProperties(item, dishDto);
            Long categoryId = item.getCategoryId();//拿到分类id
            Category category = categoryService.getById(categoryId);
            if (category!=null){
                String categoryName = category.getName();
                dishDto.setCategoryName(categoryName);
            }
            //当前菜品的id
            Long dishId = item.getId();
            //构造条件查询id对应的信息
            LambdaQueryWrapper<DishFlavor> dishFlavorLambdaQueryWrapper = new LambdaQueryWrapper<>();
            dishFlavorLambdaQueryWrapper.eq(DishFlavor::getDishId,dishId);
            //查菜品对应的口味信息
            List<DishFlavor> dishFlavorList = dishFlavorService.list(dishFlavorLambdaQueryWrapper);
            dishDto.setFlavors(dishFlavorList);
            return dishDto;
        }).collect(Collectors.toList());
        return R.success(dishDtoList);
    }
}
