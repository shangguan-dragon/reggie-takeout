package com.guo.ruiji.controller;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.guo.ruiji.common.BaseContext;
import com.guo.ruiji.common.R;
import com.guo.ruiji.dto.OrdersDto;
import com.guo.ruiji.entity.OrderDetail;
import com.guo.ruiji.entity.Orders;
import com.guo.ruiji.entity.ShoppingCart;
import com.guo.ruiji.service.IOrderDetailService;
import com.guo.ruiji.service.IOrdersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 订单表 前端控制器
 * </p>
 *
 * @author guo
 * @since 2022-04-18
 */
@RestController
@RequestMapping("/order")
public class OrdersController {

    @Autowired
    private IOrdersService ordersService;
    @Autowired
    private IOrderDetailService orderDetailService;

    /**
     * 用户下单
     *
     * @param orders
     * @return
     */
    @PostMapping("/submit")
    public R<String> submit(@RequestBody Orders orders) {
        ordersService.submit(orders);
        return R.success("下单成功");
    }

    /**
     * 用户查看自己的订单
     *
     * @param page
     * @param pageSize
     * @return
     */
    @GetMapping("/userPage")
    public R<Page<Orders>> userPage(int page, int pageSize) {
        Page<Orders> ordersPage = ordersService.userPage(page, pageSize);
        return R.success(ordersPage);
    }


    /**
     * 管理员后台查询订单列表
     *
     * @param page
     * @param pageSize
     * @param number
     * @param beginTime
     * @param endTime
     * @return
     */
    @GetMapping("/page")
    public R<Page<OrdersDto>> empPage(int page, int pageSize, String number,
                                      LocalDateTime beginTime,
                                      LocalDateTime endTime) {
        Page<OrdersDto> empPage = ordersService.empPage(page, pageSize, number, beginTime, endTime);
        return R.success(empPage);
    }

    /**
     * 派送订单
     *
     * @param orders
     * @return
     */
    @PutMapping
    public R<String> update(@RequestBody Orders orders) {
        ordersService.updateById(orders);
        return R.success("操作成功");
    }

    @PostMapping("/again")
    public R<String> again(@RequestBody Orders order) {
        ordersService.again(order);
        return R.success("再来一单啊");
    }
}
