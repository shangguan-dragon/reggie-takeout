package com.guo.ruiji.controller;


import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.guo.ruiji.common.CustomException;
import com.guo.ruiji.common.R;
import com.guo.ruiji.dto.SetmealDto;
import com.guo.ruiji.entity.Category;
import com.guo.ruiji.entity.Dish;
import com.guo.ruiji.entity.Setmeal;
import com.guo.ruiji.entity.SetmealDish;
import com.guo.ruiji.service.ICategoryService;
import com.guo.ruiji.service.IDishService;
import com.guo.ruiji.service.ISetmealDishService;
import com.guo.ruiji.service.ISetmealService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 套餐 前端控制器
 * </p>
 *
 * @author guo
 * @since 2022-04-18
 */
@Slf4j
@RestController
@RequestMapping("/setmeal")
public class SetmealController {

    @Autowired
    private ISetmealService setmealService;

    @Autowired
    private ISetmealDishService setmealDishService;

    @Autowired
    private ICategoryService categoryService;

    @Autowired
    private IDishService dishService;

    /**
     * 员工信息分页查询
     *
     * @param page     当前查询页码
     * @param pageSize 每页展示记录数
     * @param name     套餐名 - 可选参数
     * @return
     */
    @GetMapping("/page")
    public R<Page> page(int page, int pageSize, String name) {
        log.info("page = {},pageSize = {},name = {}", page, pageSize, name);
        //构造分页构造器
        Page<Setmeal> pageInfo = new Page<>(page, pageSize);
        Page<SetmealDto> setmealDtoPage = new Page<>();

        //构造条件构造器
        LambdaQueryWrapper<Setmeal> queryWrapper = new LambdaQueryWrapper<>();
        //添加过滤条件
        queryWrapper.like(StringUtils.isNotBlank(name), Setmeal::getName, name);
        //添加排序条件
        queryWrapper.orderByDesc(Setmeal::getUpdateTime);
        //只查isDelete=0的数据
        queryWrapper.eq(Setmeal::getIsDeleted, 0);

        //执行查询
        setmealService.page(pageInfo, queryWrapper);
        //对象拷贝,分页数据
        BeanUtil.copyProperties(pageInfo, setmealDtoPage, "records");
        List<Setmeal> records = pageInfo.getRecords();
        //两个数据的集合
        List<SetmealDto> list = records.stream().map(item -> {
            SetmealDto setmealDto = new SetmealDto();//此时其他属性除categoryName为null，需拷贝
            BeanUtil.copyProperties(item, setmealDto);
            Long categoryId = item.getCategoryId();//拿到分类id
            Category category = categoryService.getById(categoryId);
            String categoryName = category.getName();
            setmealDto.setCategoryName(categoryName);
            return setmealDto;
        }).collect(Collectors.toList());
        setmealDtoPage.setRecords(list);
        return R.success(setmealDtoPage);
    }

    /**
     * 新增套餐
     *
     * @param setmealDto
     * @return
     */
    @PostMapping
    public R<String> save(@RequestBody SetmealDto setmealDto) {
        log.info(setmealDto.toString());
        setmealService.saveWithDish(setmealDto);
        return R.success("新增套餐成功");
    }

    /**
     * 根据id查套餐信息
     *
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    public R<SetmealDto> getByIdWithFlavor(@PathVariable Long id) {
        SetmealDto setmealDto = setmealService.getByIdWithDish(id);
        if (setmealDto != null) {
            return R.success(setmealDto);
        }
        return R.error("未查找到套餐信息");
    }

    /**
     * 修改套餐和口味信息
     *
     * @param setmealDto
     * @return
     */
    @PutMapping
    public R<String> update(@RequestBody SetmealDto setmealDto) {
        setmealService.updateWithDish(setmealDto);
        return R.success("修改套餐信息成功");
    }

    /**
     * （可批量）1启售  0停售
     *
     * @param flag
     * @return
     */
    @PostMapping("/status/{flag}")
    public R<String> status(@PathVariable Integer flag, String[] ids) {
        if (ids == null || ids.length == 0) {
            throw new CustomException("参数有误");
        }
        LambdaUpdateWrapper<Setmeal> updateWrapper = new LambdaUpdateWrapper<>();
        // set()为sql中的set,in()为sql中的in
        updateWrapper.set(Setmeal::getStatus, flag).in(Setmeal::getId, ids);
        setmealService.update(updateWrapper);
        return R.success(flag == 0 ? "已停售" : "已启售");
    }

    /**
     * 批量删除套餐，删除单个套餐
     *
     * @param
     * @return
     */
    @DeleteMapping
    public R<String> delete(String[] ids) {
        if (ids == null || ids.length == 0) {
            throw new CustomException("参数有误");
        }
        List<Setmeal> setmeals = setmealService.listByIds(Arrays.asList(ids));
        for (Setmeal setmeal : setmeals) {
            if (setmeal.getStatus() != 0) {
                throw new CustomException(setmeal.getName() + "为启售状态，不能删除");
            }
        }
        LambdaUpdateWrapper<Setmeal> updateWrapper = new LambdaUpdateWrapper<>();
        // set()为sql中的set,in()为sql中的in
        updateWrapper.set(Setmeal::getIsDeleted, 1).in(Setmeal::getId, ids);
        setmealService.update(updateWrapper);
        return R.success("删除成功");
    }

    /**
     * 根据条件查询套餐数据
     *
     * @param setmeal
     * @return
     */
    @GetMapping("/list")
    public R<List<Setmeal>> list(Setmeal setmeal) {
        LambdaQueryWrapper<Setmeal> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(!StrUtil.isBlankIfStr(setmeal.getCategoryId()), Setmeal::getCategoryId, setmeal.getCategoryId());
        queryWrapper.eq(!StrUtil.isBlankIfStr(setmeal.getStatus()), Setmeal::getStatus, setmeal.getStatus());
        queryWrapper.orderByDesc(Setmeal::getUpdateTime);
        List<Setmeal> list = setmealService.list(queryWrapper);
        return R.success(list);
    }

    /**
     * 查看套餐详情
     *
     * @param id
     * @return
     */
    @GetMapping("/dish/{id}")
    public R<List<SetmealDish>> getById(@PathVariable Long id) {
        LambdaQueryWrapper<SetmealDish> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(SetmealDish::getSetmealId, id);
        List<SetmealDish> list = setmealDishService.list(queryWrapper);
        List<SetmealDish> list2 = new ArrayList<>();
        for (SetmealDish setmealDish : list) {
            Long dishId = setmealDish.getDishId();
            Dish dish = dishService.getById(dishId);
            setmealDish.setImage(dish.getImage());
            list2.add(setmealDish);
        }
        return R.success(list2);
    }

}
