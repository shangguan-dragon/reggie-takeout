package com.guo.ruiji.mapper;

import com.guo.ruiji.entity.Employee;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 员工信息 Mapper 接口
 * </p>
 *
 * @author guo
 * @since 2022-04-18
 */
public interface EmployeeMapper extends BaseMapper<Employee> {

}
