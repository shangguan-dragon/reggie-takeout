package com.guo.ruiji.mapper;

import com.guo.ruiji.entity.Category;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 菜品及套餐分类 Mapper 接口
 * </p>
 *
 * @author guo
 * @since 2022-04-18
 */
public interface CategoryMapper extends BaseMapper<Category> {

}
