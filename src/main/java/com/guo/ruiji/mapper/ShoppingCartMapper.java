package com.guo.ruiji.mapper;

import com.guo.ruiji.entity.ShoppingCart;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 购物车 Mapper 接口
 * </p>
 *
 * @author guo
 * @since 2022-04-18
 */
public interface ShoppingCartMapper extends BaseMapper<ShoppingCart> {

}
