package com.guo.ruiji.mapper;

import com.guo.ruiji.entity.AddressBook;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 地址管理 Mapper 接口
 * </p>
 *
 * @author guo
 * @since 2022-04-18
 */
public interface AddressBookMapper extends BaseMapper<AddressBook> {

}
