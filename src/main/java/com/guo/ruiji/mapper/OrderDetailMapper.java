package com.guo.ruiji.mapper;

import com.guo.ruiji.entity.OrderDetail;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 订单明细表 Mapper 接口
 * </p>
 *
 * @author guo
 * @since 2022-04-18
 */
public interface OrderDetailMapper extends BaseMapper<OrderDetail> {

}
