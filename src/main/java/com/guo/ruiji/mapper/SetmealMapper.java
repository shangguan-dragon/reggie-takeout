package com.guo.ruiji.mapper;

import com.guo.ruiji.entity.Setmeal;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 套餐 Mapper 接口
 * </p>
 *
 * @author guo
 * @since 2022-04-18
 */
public interface SetmealMapper extends BaseMapper<Setmeal> {

}
