package com.guo.ruiji.mapper;

import com.guo.ruiji.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户信息 Mapper 接口
 * </p>
 *
 * @author guo
 * @since 2022-04-18
 */
public interface UserMapper extends BaseMapper<User> {

}
