package com.guo.ruiji.mapper;

import com.guo.ruiji.entity.Orders;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 订单表 Mapper 接口
 * </p>
 *
 * @author guo
 * @since 2022-04-18
 */
public interface OrdersMapper extends BaseMapper<Orders> {

}
