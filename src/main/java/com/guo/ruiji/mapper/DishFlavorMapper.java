package com.guo.ruiji.mapper;

import com.guo.ruiji.entity.DishFlavor;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 菜品口味关系表 Mapper 接口
 * </p>
 *
 * @author guo
 * @since 2022-04-18
 */
public interface DishFlavorMapper extends BaseMapper<DishFlavor> {

}
