package com.guo.ruiji.mapper;

import com.guo.ruiji.entity.Dish;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 菜品管理 Mapper 接口
 * </p>
 *
 * @author guo
 * @since 2022-04-18
 */
public interface DishMapper extends BaseMapper<Dish> {

}
