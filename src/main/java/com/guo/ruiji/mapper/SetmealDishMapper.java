package com.guo.ruiji.mapper;

import com.guo.ruiji.entity.SetmealDish;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 套餐菜品关系 Mapper 接口
 * </p>
 *
 * @author guo
 * @since 2022-04-18
 */
public interface SetmealDishMapper extends BaseMapper<SetmealDish> {

}
