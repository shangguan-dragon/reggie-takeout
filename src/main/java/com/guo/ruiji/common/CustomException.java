package com.guo.ruiji.common;

/**
 * @authod shiwen <https://gitee.com/shiwen828>
 * @date 2022/4/4 19:44
 */

/**
 * 自定义业务异常类
 */
public class CustomException extends RuntimeException{
    public CustomException(String message){
        super(message);
    }
}
