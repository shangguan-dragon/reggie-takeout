package com.guo.ruiji.common;

import cn.hutool.core.io.FileUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.util.UUID;

/**
 * 文件的上传与下载类
 */
@Slf4j
@RestController
@RequestMapping("common")
public class CommonController {

    @Value("${files.upload.path}")
    private String basePath;

    /**
     * 文件上传
     *
     * @param file
     * @return
     */
    @PostMapping("/upload")
    public R<String> upload(MultipartFile file) {
        //file是一个临时文件，需要转存到指定位置，否则本次请求完成后临时文件会删除
        log.info(file.toString());

        //原始文件名
        String originalFilename = file.getOriginalFilename();//abc.jpg
        assert originalFilename != null;
        String suffix = originalFilename.substring(originalFilename.lastIndexOf("."));//.jpg

        //使用UUID重新生成文件名，防止文件名称重复造成文件覆盖
        String fileName = UUID.randomUUID().toString() + suffix;//sdfadgdfsgasfgf.jpg

        //创建目录对象
        File dir = new File(basePath);
        if (!dir.exists()) {
            //目录不存在，则创建
            dir.mkdirs();
        }

        try {
            //将临时文件转存到指定位置
            file.transferTo(new File(basePath + fileName));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return R.success(fileName);
    }

    @GetMapping("/download")
    public void download(String name, HttpServletResponse response){
        String pathName=basePath+name;
        try {
            //输出流，通过输出流将文件写回浏览器
            ServletOutputStream os = response.getOutputStream();
            response.setContentType("image/jpeg");
            // 读取文件的字节流
            os.write(FileUtil.readBytes(pathName));
            os.flush();
            os.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
