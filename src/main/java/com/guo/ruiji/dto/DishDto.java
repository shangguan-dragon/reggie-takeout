package com.guo.ruiji.dto;


import com.guo.ruiji.entity.Dish;
import com.guo.ruiji.entity.DishFlavor;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * @authod guo
 * @date 2022/4/6 19:43
 */
@Data
public class DishDto extends Dish {
    //菜品口味集合
    private List<DishFlavor> flavors = new ArrayList<>();
    //菜品分类名称
    private String categoryName;

    private Integer copies;
}
