package com.guo.ruiji.dto;

import com.guo.ruiji.entity.Setmeal;
import com.guo.ruiji.entity.SetmealDish;
import lombok.Data;

import java.util.List;

/**
 * @authod shiwen <https://gitee.com/shiwen828>
 * @date 2022/4/7 20:19
 */
@Data
public class SetmealDto extends Setmeal {

    private List<SetmealDish> setmealDishes;//套餐关联的菜品集合

    private String categoryName;//分类名称
}
