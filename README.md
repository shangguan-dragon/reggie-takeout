# 瑞吉外卖

#### 介绍
修改传智播客教育下的瑞吉外卖项目，所有功能均已实现
引入hutool工具包，该项目主要学习了lambda编程方式

#### 软件架构
软件架构说明


#### 安装教程

1. 首先使用maven导入项目

2. 在resources文件夹下有mysql数据文件

3. 在application.yml修改数据库配置文件

4. 文件图片配置也要更改

   ```yml
   # 文件上传服务器保存路径
   files:
     upload:
       path: D:\IdeaProjects\ruijiwaimai\src\main\resources\files\
       # 以上路径更改为你自己的绝对路径，最后不要忘记\
   ```

5.  如果有问题可以评论区提出

#### 使用说明

1.  手机端登录界面，新增倒计时演示
![输入图片说明](doc/image1.png)
2.  验证码使用hutool随机生成四位数字验证码
![输入图片说明](doc/image.png)
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
